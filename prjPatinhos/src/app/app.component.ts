import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title="app"
  numPatinhos: any = '';
  result: string = '';
  isRaveModeActive: boolean = false;
  lightsClass: string = '';
 
  countPatinhos(): void{


   this.numPatinhos = Math.trunc(this.numPatinhos); 
    
    if (isNaN(this.numPatinhos) || this.numPatinhos <= 0 ) {
      this.result = 'Insira um numero valido';
      return;
    }
 
 
    let result = '';
    
    for (let i = this.numPatinhos; i >= 2; i--) {
      result += `${i} patinhos foram passear<br>
       Além das montanhas, para brincar<br>
       A mamãe gritou: Quá, quá, quá, quá<br>
       Mas só ${i - 1} ${(i - 1 == 1 ? 'patinho voltou' : 'patinhos voltaram')} de lá<br><br>`
    }

    result += `1 patinho foi passear<br>Além das montanhas para brincar<br>
      A mamãe gritou: Quá, quá, quá, quá<br>
      Mas nenhum patinho voltou de lá<br><br>
      Poxa, a mamãe patinha ficou tão triste naquele dia<br>`

    result += `Aonde será que ${(this.numPatinhos == 1 ? 'estava o seu filhotinho?' : 'estavam os seus filhotinhos?')}<br>
    Mas essa história vai ter um final feliz<br>
    Sabe por quê?<br><br>
    A mamãe patinha foi procurar<br>
    Além das montanhas, na beira do mar<br>
    A mamãe gritou: Quá, quá, quá, quá!<br>`

    if (this.numPatinhos == 1) {
      result += 'E o patinho voltou de lá';
    } else {
      result += `E os ${this.numPatinhos} patinhos voltaram de lá`;
    }

    this.result = result;
    this.numPatinhos = '';
  }

  toggleRaveMode() {
    this.isRaveModeActive = !this.isRaveModeActive;
    if (this.isRaveModeActive) {
      this.startRave();
    } else {
      this.stopRave();
    }
  }

  startRave() {
    this.lightsClass = 'flashing-lights';  
  }

  stopRave() {
    this.lightsClass = '';
  }

}